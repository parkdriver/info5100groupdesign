/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Food.FoodDirectory;
import Business.Enterprise.DeliveryServiceEnterprise;
import Business.Enterprise.RestaurantEnterprise;

/**
 *
 * @author mac
 */
//Customer send order to restaurant
public class OrderWorkRequest {
    private FoodDirectory foodDirectory;
    private DeliveryServiceEnterprise deliveryService;
    private RestaurantEnterprise restaurant;
    private int foodEvalutaion;
    private int deliveryEvaluation;

    public OrderWorkRequest(DeliveryServiceEnterprise deliveryService, RestaurantEnterprise restaurant) {
        this.deliveryService = deliveryService;
        this.restaurant = restaurant;
        foodDirectory = new FoodDirectory();
    }
    
}
