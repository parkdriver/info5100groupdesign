/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Food;

import java.text.DecimalFormat;

/**
 *
 * @author mac
 */
public class Food {
    private String name;
    private float price;
    private float evaluate;
    private int sales = 0;
    DecimalFormat df = new DecimalFormat("0.0");

    public Food(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(int evaluate) {
        this.evaluate = Float.parseFloat(df.format((this.evaluate*(sales-1) + evaluate)/sales)); 
    }

    public int getSales() {
        return sales;
    }

    public void addSales() {
        sales++;
    }
    
    
}
