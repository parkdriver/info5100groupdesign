/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.SysAdminRole;

import Business.EcoSystem;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author liumingyu
 */
public class SysAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SysAdminWorkAreaJPanel
     */
    private JPanel rightpanel;
    private EcoSystem ecoSystem;
    DefaultMutableTreeNode selectednode;
    DefaultTreeModel model;
    public SysAdminWorkAreaJPanel(JPanel rightpanel, EcoSystem ecoSystem) {
        initComponents();
        this.rightpanel = rightpanel;
        this.ecoSystem = ecoSystem;
        model = (DefaultTreeModel) jTree1.getModel();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        NetWorkBtn = new javax.swing.JButton();
        ManageDeliveryServiceBtn = new javax.swing.JButton();
        ManageEnterpriseManagerBtn = new javax.swing.JButton();
        ManageRestaurantBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        addBtn = new javax.swing.JButton();
        removeBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        nameTxt = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(249, 246, 246));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(153, 153, 153)));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Interface/SysAdminRole/takeout-top.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 866, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 6, 870, 210));

        jPanel2.setBackground(new java.awt.Color(249, 246, 246));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(153, 153, 153)));

        NetWorkBtn.setBackground(new java.awt.Color(255, 255, 255));
        NetWorkBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        NetWorkBtn.setText("Manage NetWork");
        NetWorkBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NetWorkBtnActionPerformed(evt);
            }
        });

        ManageDeliveryServiceBtn.setBackground(new java.awt.Color(255, 255, 255));
        ManageDeliveryServiceBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        ManageDeliveryServiceBtn.setText("Manage Delivery Service");
        ManageDeliveryServiceBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageDeliveryServiceBtnActionPerformed(evt);
            }
        });

        ManageEnterpriseManagerBtn.setBackground(new java.awt.Color(255, 255, 255));
        ManageEnterpriseManagerBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        ManageEnterpriseManagerBtn.setText("Manage Enterprise Manager");
        ManageEnterpriseManagerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageEnterpriseManagerBtnActionPerformed(evt);
            }
        });

        ManageRestaurantBtn.setBackground(new java.awt.Color(255, 255, 255));
        ManageRestaurantBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        ManageRestaurantBtn.setText("Manage Restaurant");
        ManageRestaurantBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageRestaurantBtnActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jTree1);

        addBtn.setText("Add");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        removeBtn.setText("Remove");
        removeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeBtnActionPerformed(evt);
            }
        });

        jLabel2.setText("Name");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addComponent(ManageDeliveryServiceBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(132, 132, 132)
                        .addComponent(ManageRestaurantBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(734, 734, 734))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(340, 340, 340)
                                .addComponent(ManageEnterpriseManagerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(removeBtn)
                                    .addComponent(addBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(215, 215, 215)
                                .addComponent(NetWorkBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(22, 22, 22)
                                        .addComponent(jLabel2)))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 2, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(NetWorkBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(nameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(removeBtn)))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ManageDeliveryServiceBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ManageRestaurantBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(53, 53, 53)
                        .addComponent(ManageEnterpriseManagerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 221, 870, 390));
    }// </editor-fold>//GEN-END:initComponents

    private void NetWorkBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NetWorkBtnActionPerformed
        // TODO add your handling code here:
        ManageLocationJPanel mlpanel = new ManageLocationJPanel(rightpanel, ecoSystem);
        this.rightpanel.add(mlpanel);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
    }//GEN-LAST:event_NetWorkBtnActionPerformed

    private void ManageDeliveryServiceBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageDeliveryServiceBtnActionPerformed
        // TODO add your handling code here:
        ManageDeliveryServiceJPanel mdpanel = new ManageDeliveryServiceJPanel(rightpanel, ecoSystem);
        this.rightpanel.add(mdpanel);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
    }//GEN-LAST:event_ManageDeliveryServiceBtnActionPerformed

    private void ManageEnterpriseManagerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageEnterpriseManagerBtnActionPerformed
        // TODO add your handling code here:
        ManageEnterpriseManagerJPanel mempanel = new ManageEnterpriseManagerJPanel(rightpanel, ecoSystem);
        this.rightpanel.add(mempanel);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
    }//GEN-LAST:event_ManageEnterpriseManagerBtnActionPerformed

    private void ManageRestaurantBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageRestaurantBtnActionPerformed
        // TODO add your handling code here:
        ManageRestaurantJPanel mrpanel = new ManageRestaurantJPanel(rightpanel, ecoSystem);
        this.rightpanel.add(mrpanel);
        CardLayout layout = (CardLayout)rightpanel.getLayout();
        layout.next(rightpanel);
    }//GEN-LAST:event_ManageRestaurantBtnActionPerformed

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBtnActionPerformed
        selectednode = (DefaultMutableTreeNode)jTree1.getLastSelectedPathComponent();
        if(selectednode != null){
            selectednode.insert(new DefaultMutableTreeNode(nameTxt.getText()), selectednode.getIndex(selectednode.getLastChild()));
            model.reload(selectednode);
        }
        nameTxt.setText("");
    }//GEN-LAST:event_addBtnActionPerformed

    private void removeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeBtnActionPerformed
       selectednode = (DefaultMutableTreeNode) jTree1.getLastSelectedPathComponent();
       if(selectednode != null){
           DefaultMutableTreeNode parent= (DefaultMutableTreeNode) selectednode.getParent();
           parent.remove(selectednode);
           model.reload(parent);
       }
    }//GEN-LAST:event_removeBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ManageDeliveryServiceBtn;
    private javax.swing.JButton ManageEnterpriseManagerBtn;
    private javax.swing.JButton ManageRestaurantBtn;
    private javax.swing.JButton NetWorkBtn;
    private javax.swing.JButton addBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree1;
    private javax.swing.JTextField nameTxt;
    private javax.swing.JButton removeBtn;
    // End of variables declaration//GEN-END:variables
}
