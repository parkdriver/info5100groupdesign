/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lab7.entities.Comment;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    //1). Find Average number of likes per comment.
    public void getAveragelikesPerComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        List<Integer> averageLikesList = new ArrayList<>();
        List<Integer> postIdList = new ArrayList<>();
        
        //get unique postid
        for(int i=0;i<commentList.size();i++){
            int postId = commentList.get(i).getPostId();
            if(!postIdList.contains(postId)){
                postIdList.add(postId);
            }
        }
        
        //get average
        for(int i=0;i<postIdList.size();i++){
            int sumlikes=0;
            int countlikes=0;
            int averagelikes=0;
            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getPostId()==postIdList.get(i)){
                    sumlikes+=commentList.get(j).getLikes();
                    countlikes+=1;
                }
                
            }
            if(countlikes!=0){
                averagelikes=sumlikes/countlikes;  
            }
            else if(countlikes==0){
                averagelikes=0;
            }
                     
            averageLikesList.add(averagelikes);
            
        }
        
        System.out.println("\n"+"1). Find Average number of likes per comment."+"\n");
        
        for(int i = 0; i<averageLikesList.size();i++){
            System.out.println("postId: "+postIdList.get(i)+"\t averageLikes: "
                    +averageLikesList.get(i));
            
        }
      
    }
    
//2). Find the post with most liked comments.
    public void getPostWithMostLikedComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        List<Integer> sumLikesList = new ArrayList<>();

        
        List<Integer> postIdList = new ArrayList<>();
        
        //get unique postid
        for(int i=0;i<commentList.size();i++){
            int postId = commentList.get(i).getPostId();
            if(!postIdList.contains(postId)){
                postIdList.add(postId);
            }
        }
        
        //get sum
        for(int i=0;i<postIdList.size();i++){
            int sumlikes=0;            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getPostId()==postIdList.get(i)){
                    sumlikes+=commentList.get(j).getLikes();
                }               
            }                     
            sumLikesList.add(sumlikes);            
        }
        
        //find most
        int postindex=0;
        int mostlikedpost = 0;
        for(int i=0;i<sumLikesList.size();i++){            
            if(sumLikesList.get(i)>mostlikedpost){
                mostlikedpost=sumLikesList.get(i);
                postindex=i;
            }
        }
        System.out.println("\n"+"2). Find the post with most liked comments."+"\n");
        
        System.out.println("The post with most liked comments is: "+postindex);
        
        
        
    }
    
//3). Find the post with most comments.
    public void getPostWithMostComments(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        List<Integer> postIdList = new ArrayList<>();
        
        List<Integer> commentCountList = new ArrayList<>();
        
        //get unique postid
        for(int i=0;i<commentList.size();i++){
            int postId = commentList.get(i).getPostId();
            if(!postIdList.contains(postId)){
                postIdList.add(postId);
            }
        }
        
        //get count
        for(int i=0;i<postIdList.size();i++){
            int countcomments=0;
            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getPostId()==postIdList.get(i)){
                    countcomments+=1;
                }
                
            }                    
            commentCountList.add(countcomments);
            
        }
        
        //find most comments post
        int postindex=0;
        int mostCommentPost = 0;
        for(int i=0;i<commentCountList.size();i++){            
            if(commentCountList.get(i)>mostCommentPost){
                mostCommentPost=commentCountList.get(i);
                postindex=i;
            }
        }
        System.out.println("\n"+"3). Find the post with most comments."+"\n");
        
        System.out.println("The post with most comments is: "+postindex);
        
        
    }
    
//4). Top 5 inactive users based on total posts number.
    public void getFiveInactiveUserByPost(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        List<Comment> uniqueCommentList = new ArrayList<>();
        
        Map<Integer,Integer> userPost = new HashMap<Integer,Integer>();
        
        List<Integer> userIdList = new ArrayList<>();
        
        List<Integer> postCountList = new ArrayList<>();
        
        //get unique commentList
        uniqueCommentList.add(commentList.get(0));
        for(int i=1;i<commentList.size();i++){
            if(commentList.get(i).getPostId()!=commentList.get(i-1).getPostId()/*&&
                    commentList.get(i).getUserId()!=commentList.get(i-1).getUserId()*/){
                uniqueCommentList.add(commentList.get(i));
            }
        }
        
        
        //get unique userid
        for(int i=0;i<uniqueCommentList.size();i++){
            int userId = uniqueCommentList.get(i).getUserId();
            if(!userIdList.contains(userId)){
                userIdList.add(userId);
            }
        }
        
        //count post
        for(int i=0;i<userIdList.size();i++){
            int countposts=0;
            
            for(int j=0;j<uniqueCommentList.size();j++){
                if(uniqueCommentList.get(j).getUserId()==userIdList.get(i)){
                    countposts+=1;
                }
                
            }                    
            postCountList.add(countposts);
            
        }
        
        //add to map
        for(int i =0;i<userIdList.size();i++){
            int userId=userIdList.get(i);
            int postCount = postCountList.get(i);
            userPost.put(userId, postCount);
        }
        
        //sort by post count
        //to list
        List<Map.Entry<Integer, Integer>> list = 
                new ArrayList<Map.Entry<Integer, Integer>>(userPost.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer, Integer>>() {
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o1.getValue().compareTo(o2.getValue());
          }
      });
        
        System.out.println("\n"+"4). Top 5 inactive users based on total posts number."+"\n");
        
        for (int i = 0; i < 5; i++) {
            int a=i+1;
            System.out.println("Top "+a+" inactive users based on total posts number: "
                    +list.get(i).getKey() + ", posts number: " + list.get(i).getValue());
        } 
        
        
        
        
    }
    
//5). Top 5 inactive users based on total comments they created.
    public void getFiveInactiveUserByComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        List<Integer> userIdList = new ArrayList<>();
        
        List<Integer> commentCountList = new ArrayList<>();
        
        Map<Integer,Integer> userComment = new HashMap<Integer,Integer>();
        
        //get unique userid
        for(int i=0;i<commentList.size();i++){
            int userId = commentList.get(i).getUserId();
            if(!userIdList.contains(userId)){
                userIdList.add(userId);
            }
        }
        
        //get count comments
        for(int i=0;i<userIdList.size();i++){
            int countcomments=0;
            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getUserId()==userIdList.get(i)){
                    countcomments+=1;
                }
                
            }                    
            commentCountList.add(countcomments);          
        }
        
        //add to map
        for(int i =0;i<userIdList.size();i++){
            int userId=userIdList.get(i);
            int commentCount = commentCountList.get(i);
            userComment.put(userId, commentCount);
        }
        
        //sort by comments
        //to list
        List<Map.Entry<Integer, Integer>> list = 
                new ArrayList<Map.Entry<Integer, Integer>>(userComment.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer, Integer>>() {
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o1.getValue().compareTo(o2.getValue());
          }
      });
        
        System.out.println("\n"+"5). Top 5 inactive users based on total comments they created."+"\n");
        
        for (int i = 0; i < 5; i++) {
            int a=i+1;
            System.out.println("Top "+a+" inactive users based on total comments created: "
                    +list.get(i).getKey() + ", comments created: " + list.get(i).getValue());
        } 
        
        
    }
    
//6). Top 5 inactive users overall (sum of comments, posts and likes)
    public void getFiveInactiveOverall(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        List<Comment> uniqueCommentList = new ArrayList<>();
        
        List<Integer> userIdList = new ArrayList<>();
        
        List<Integer> commentCountList = new ArrayList<>();
        
        List<Integer> postCountList = new ArrayList<>();
        
        List<Integer> sumLikesList = new ArrayList<>();
        
        List<Integer> sumList = new ArrayList<>();
        
             
        Map<Integer,Integer> userSum = new HashMap<Integer,Integer>();
        

        //get unique commentList
        uniqueCommentList.add(commentList.get(0));
        for(int i=1;i<commentList.size();i++){
            if(commentList.get(i).getPostId()!=commentList.get(i-1).getPostId()){
                uniqueCommentList.add(commentList.get(i));
            }
        }
        
        //get unique userid
        for(int i=0;i<commentList.size();i++){
            int userId = commentList.get(i).getUserId();
            if(!userIdList.contains(userId)){
                userIdList.add(userId);
            }
        }
        
        //count post
        for(int i=0;i<userIdList.size();i++){
            int countposts=0;
            
            for(int j=0;j<uniqueCommentList.size();j++){
                if(uniqueCommentList.get(j).getUserId()==userIdList.get(i)){
                    countposts+=1;
                }
                
            }                    
            postCountList.add(countposts);
            
        }
        
        //get count comments
        for(int i=0;i<userIdList.size();i++){
            int countcomments=0;
            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getUserId()==userIdList.get(i)){
                    countcomments+=1;
                }
                
            }                    
            commentCountList.add(countcomments);          
        }
        
        //get sum likes
        for(int i=0;i<userIdList.size();i++){
            int sumlikes=0;            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getUserId()==userIdList.get(i)){
                    sumlikes+=commentList.get(j).getLikes();
                }               
            }                     
            sumLikesList.add(sumlikes);            
        }
        
        //sum together
        for(int i=0;i<userIdList.size();i++){
            int allsum=sumLikesList.get(i)+commentCountList.get(i)+postCountList.get(i);
            sumList.add(allsum);
        }
        
        //add to map
        for(int i =0;i<userIdList.size();i++){
            int userId=userIdList.get(i);
            int sum = sumList.get(i);
            userSum.put(userId, sum);
        }
        
        //sort by sum
        //to list
        List<Map.Entry<Integer, Integer>> list = 
                new ArrayList<Map.Entry<Integer, Integer>>(userSum.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer, Integer>>() {
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o1.getValue().compareTo(o2.getValue());
          }
      });
        
        System.out.println("\n"+"6). Top 5 inactive users overall (sum of comments, posts and likes)"+"\n");
        
        for (int i = 0; i < 5; i++) {
            int a=i+1;
            System.out.println("Top "+a+" inactive users based on sum: "
                    +list.get(i).getKey() + ", sum: " + list.get(i).getValue());
        } 
  
        
    }
//7). Top 5 proactive users overall (sum of comments, posts and likes)
    public void getFiveProactiveOverall(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        List<Comment> uniqueCommentList = new ArrayList<>();
        
        List<Integer> userIdList = new ArrayList<>();
        
        List<Integer> commentCountList = new ArrayList<>();
        
        List<Integer> postCountList = new ArrayList<>();
        
        List<Integer> sumLikesList = new ArrayList<>();
        
        List<Integer> sumList = new ArrayList<>();
        
             
        Map<Integer,Integer> userSum = new HashMap<Integer,Integer>();
        

        //get unique commentList
        uniqueCommentList.add(commentList.get(0));
        for(int i=1;i<commentList.size();i++){
            if(commentList.get(i).getPostId()!=commentList.get(i-1).getPostId()){
                uniqueCommentList.add(commentList.get(i));
            }
        }
        
        //get unique userid
        for(int i=0;i<commentList.size();i++){
            int userId = commentList.get(i).getUserId();
            if(!userIdList.contains(userId)){
                userIdList.add(userId);
            }
        }
        
        //count post
        for(int i=0;i<userIdList.size();i++){
            int countposts=0;
            
            for(int j=0;j<uniqueCommentList.size();j++){
                if(uniqueCommentList.get(j).getUserId()==userIdList.get(i)){
                    countposts+=1;
                }
                
            }                    
            postCountList.add(countposts);
            
        }
        
        //get count comments
        for(int i=0;i<userIdList.size();i++){
            int countcomments=0;
            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getUserId()==userIdList.get(i)){
                    countcomments+=1;
                }
                
            }                    
            commentCountList.add(countcomments);          
        }
        
        //get sum likes
        for(int i=0;i<userIdList.size();i++){
            int sumlikes=0;            
            for(int j=0;j<commentList.size();j++){
                if(commentList.get(j).getUserId()==userIdList.get(i)){
                    sumlikes+=commentList.get(j).getLikes();
                }               
            }                     
            sumLikesList.add(sumlikes);            
        }
        
        //sum together
        for(int i=0;i<userIdList.size();i++){
            int allsum=sumLikesList.get(i)+commentCountList.get(i)+postCountList.get(i);
            sumList.add(allsum);
        }
        
        //add to map
        for(int i =0;i<userIdList.size();i++){
            int userId=userIdList.get(i);
            int sum = sumList.get(i);
            userSum.put(userId, sum);
        }
        
        //sort by sum
        //to list
        List<Map.Entry<Integer, Integer>> list = 
                new ArrayList<Map.Entry<Integer, Integer>>(userSum.entrySet());
        
        list.sort(new Comparator<Map.Entry<Integer, Integer>>() {
          public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
              return o2.getValue().compareTo(o1.getValue());
          }
      });
        
        System.out.println("\n"+"7). Top 5 proactive users overall (sum of comments, posts and likes)"+"\n");
        
        for (int i = 0; i < 5; i++) {
            int a=i+1;
            System.out.println("Top "+a+" proactive users based on sum: "
                    +list.get(i).getKey() + ", sum: " + list.get(i).getValue());
        }
        
        
        
    }
    
    
    // find 5 comments which have the most likes
    // TODO
    public void getFiveLikedComments(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        //Map<Integer, User> users = DataStore.getInstance().getUsers();
        
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        //sort by likes
        Collections.sort(commentList,new Comparator<Comment>(){
            public int compare(Comment c1, Comment c2){
                return Double.compare(c2.getLikes(), c1.getLikes());
                
            }
        });
        
        for(int i = 0; i<comments.size()&& i<5;i++){
            System.out.println(commentList.get(i));
            
        }   
    }
    



}
